package com.adc.csrmagentoadapter.controller;

import com.adc.csrmagentoadapter.domain.object.Address;
import com.adc.csrmagentoadapter.domain.object.Customer;
import com.adc.csrmagentoadapter.exception.CsrMagentoException;
import com.adc.csrmagentoadapter.service.MagentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RefreshScope
@RestController
public class CsrMagentoController {

    @Value("${message:Hello default}")
    private String message;

    @Value("${url: default}")
    private String url;

    @Autowired
    private MagentoService magentoService;

    @PostMapping(value = "/customer", produces = MediaType.TEXT_PLAIN_VALUE)
    public String insertCustomerDetails(@RequestHeader(value="countryCode") String countryCode, @RequestHeader(value="languageCode") String languageCode, @RequestHeader(value="consumerKey") String consumerKey, @RequestHeader(value="accessToken") String accessToken, @Valid @RequestBody Customer customer ) throws CsrMagentoException {
    try{
        System.out.println("message:"+message + "url :"+url);
        return magentoService.insertCustomerDetails(customer);
    }catch (Exception e){
        throw new CsrMagentoException(e.getMessage());
    }
    //return this.message;
    }

    @PutMapping(value = "customer/{b2cCustomerId}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String upsertCustomerDetails(@PathVariable(value = "b2cCustomerId") String b2cCustomerId, @RequestHeader(value="countryCode") String countryCode, @RequestHeader(value="languageCode") String languageCode, @RequestHeader(value="consumerKey") String consumerKey, @RequestHeader(value="accessToken") String accessToken,@Valid @RequestBody Customer customer ) throws CsrMagentoException{
    try{
        return magentoService.upsertCustomerDetails(customer);
    }catch (Exception e){
        throw new CsrMagentoException(e.getMessage());
    }
    }

    @PostMapping(value = "customer/{b2cCustomerId}/address", produces = MediaType.TEXT_PLAIN_VALUE)
    public String insertAddressDetails(@PathVariable(value = "b2cCustomerId") String b2cCustomerId, @RequestHeader(value="countryCode") String countryCode, @RequestHeader(value="languageCode") String languageCode, @RequestHeader(value="consumerKey") String consumerKey, @RequestHeader(value="accessToken") String accessToken,@Valid @RequestBody Address address) throws CsrMagentoException{
    try{
        return magentoService.insertAddressDetails(address);
    }catch (Exception e){
        throw new CsrMagentoException(e.getMessage());
    }
        //return client.helloWorld();
    }

    @PutMapping(value = "customer/{b2cCustomerId}/address/{b2cAddressId}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String upsertAddressDetails(@PathVariable(value = "b2cCustomerId") String b2cCustomerId, @PathVariable(value = "b2cAddressId") String b2cAddressId, @RequestHeader(value="countryCode") String countryCode, @RequestHeader(value="languageCode") String languageCode, @RequestHeader(value="consumerKey") String consumerKey, @RequestHeader(value="accessToken") String accessToken,@Valid @RequestBody Address address) throws CsrMagentoException{
    try{
        return magentoService.insertAddressDetails(address);
    }catch (Exception e){
        throw new CsrMagentoException(e.getMessage());
    }
    }

    public String getUrl(){
        return url;
    }

}
