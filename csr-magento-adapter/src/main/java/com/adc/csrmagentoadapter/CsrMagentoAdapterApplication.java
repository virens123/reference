package com.adc.csrmagentoadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CsrMagentoAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrMagentoAdapterApplication.class, args);
	}
}
