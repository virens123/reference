package com.adc.csrmagentoadapter.domain.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Address {
    @NotNull
    @JsonProperty("csrCustomerId")
    private String csrCustomerId;
    @NotNull
    @JsonProperty("addressId")
    private String addressId;
    @NotNull
    @JsonProperty("prefix")
    private String prefix;
    @NotNull
    @JsonProperty("firstName")
    private String firstName;
    @NotNull
    @JsonProperty("lastName")
    private String lastName;
    @NotNull
    @JsonProperty("street")
    private String street;
    @NotNull
    @JsonProperty("city")
    private String city;
    @NotNull
    @JsonProperty("postCode")
    private String postCode;
    @JsonProperty("region")
    private String region;
    @JsonProperty("countryId")
    private String countryId;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("fax")
    private String fax;
    @JsonProperty("vatNumber")
    private String vatNumber;
    @JsonProperty("customerAddressLabel")
    private String customerAddressLabel;
    @JsonProperty("pickpointTerminalId")
    private String pickpointTerminalId;
    @JsonProperty("pickpointTerminalName")
    private String pickpointTerminalName;
    @JsonProperty("missingVerification")
    private Boolean missingVerification;
    @NotNull
    @JsonProperty("isDefaultBilling")
    private Boolean isDefaultBilling;
    @NotNull
    @JsonProperty("isDefaultShipping")
    private Boolean isDefaultShipping;
    @NotNull
    @JsonProperty("company")
    private String company;
}
