package com.adc.csrmagentoadapter.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name ="magentoClient",url = "#{MAGENTO_URL}")
public interface MagentoClient {
    @GetMapping(value = "/test")
    public String invokeMagento();
}