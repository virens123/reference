package com.abbott.adc.eventapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@ToString
public class Event {

    @JsonProperty("eventID")
    private String eventID;
    @JsonProperty("messageGroupID")
    private String messageGroupID;
    @JsonProperty("eventCreationTime")
    private String eventCreationTime;
    @JsonProperty("eventCorrelationID")
    private String eventCorrelationID;
    @JsonProperty("entityKeys")
    private List<EntityKey> entityKeys = null;
    @JsonProperty("entityAttributes")
    private List<EntityAttribute> entityAttributes = null;

}