package com.abbott.adc.eventapi.controller;

import com.abbott.adc.eventapi.model.Event;
import com.abbott.adc.eventapi.publisher.JmsPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RefreshScope
@RestController
@RequestMapping(value = "/{country}/{language}/v1.0")
public class EventApiController {

    @Autowired
    JmsPublisher publisher;

    @PostMapping(value = "/events",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public void manageEvents(@PathVariable(value= "country") String country , @PathVariable(value= "language") String language, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken,@RequestHeader(value = "eventDomain") String eventDomain,@RequestHeader(value = "eventType") String eventType,@RequestHeader(value = "eventSubType") String eventSubType,@RequestHeader(value = "eventOriginationSystem") String eventOriginationSystem, @RequestBody Event event){
    publisher.send(event);
    }
}
