package com.abbott.adc.service.event.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EntityKey {

    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
}