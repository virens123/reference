package com.abbott.adc.service.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.abbott.adc.service.event.listener.JmsConsumer;

@SpringBootApplication
public class ServiceApplication {

	@Autowired
	static JmsConsumer jmsConsumer;

	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
		jmsConsumer.consumeEvent();
	}
}
