package com.abbott.adc.service.event.listener;

import javax.jms.JMSException;

import org.springframework.stereotype.Component;

@Component
public class JmsConsumer {

	public static void consumeEvent() {
		// the message in the topic before this subscriber starts will not be
		// picked up.
		EventConsumer eventListener = new EventConsumer("tcp://localhost:61616", "admin","admin");
		eventListener.setDestinationTopic(true);
		eventListener.setDestinationName("VirtualTopic.jsa-topic");		

		try {
			eventListener.run();
		} catch (JMSException e) {

			e.printStackTrace();
		}
	}

}
