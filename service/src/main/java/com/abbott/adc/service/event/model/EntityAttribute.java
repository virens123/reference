package com.abbott.adc.service.event.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class EntityAttribute {

    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
}