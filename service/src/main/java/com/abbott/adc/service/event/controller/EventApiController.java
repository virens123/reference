package com.abbott.adc.service.event.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abbott.adc.service.event.model.Event;
import com.abbott.adc.service.event.publisher.EventPublisher;
import com.fasterxml.jackson.databind.ObjectMapper;

@RefreshScope
@RestController
@RequestMapping(value = "/{country}/{language}/v1.0")
public class EventApiController {

    @Autowired
    EventPublisher eventPublisher;
    
    @Value("${jsa.activemq.topic}")
	private String topic;
     

    @PostMapping(value = "/events",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public void manageEvents(@PathVariable(value= "country") String country , @PathVariable(value= "language") String language, @RequestHeader(value = "consumerKey") String consumerKey, @RequestHeader(value = "accessToken") String accessToken,@RequestHeader(value = "eventDomain") String eventDomain,@RequestHeader(value = "eventType") String eventType,@RequestHeader(value = "eventSubType") String eventSubType,@RequestHeader(value = "eventOriginationSystem") String eventOriginationSystem, @RequestBody Event event) throws Exception{
    ObjectMapper mapper = new ObjectMapper();
    eventPublisher.setup(false, true, topic);
    eventPublisher.sendMessage(mapper.writeValueAsString(event));
    }
}
